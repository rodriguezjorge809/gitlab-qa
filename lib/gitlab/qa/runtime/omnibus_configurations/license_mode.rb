# frozen_string_literal: true

module Gitlab
  module QA
    module Runtime
      module OmnibusConfigurations
        class LicenseMode < Default
          def configuration
            <<~OMNIBUS
              gitlab_rails['env'] = { 'GITLAB_LICENSE_MODE' => 'test', 'CUSTOMER_PORTAL_URL' => 'https://customers.staging.gitlab.com' }
            OMNIBUS
          end
        end
      end
    end
  end
end
