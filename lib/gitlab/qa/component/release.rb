# frozen_string_literal: true

module Gitlab
  module QA
    module Component
      class Release < Staging
        ADDRESS = 'https://release.gitlab.net'
      end
    end
  end
end
