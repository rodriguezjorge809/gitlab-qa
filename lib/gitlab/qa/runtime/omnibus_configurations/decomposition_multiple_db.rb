# frozen_string_literal: true

module Gitlab
  module QA
    module Runtime
      module OmnibusConfigurations
        class DecompositionMultipleDb < Default
          def configuration
            # HACK: commenting commands out as these commands should be run *after* the first
            # reconfiguration (see first command in #exec_commands)
            <<~OMNIBUS
              #gitlab_rails['databases']['main']['enable'] = true
              #gitlab_rails['databases']['ci']['enable'] = true
              #gitlab_rails['databases']['ci']['db_database'] = 'gitlabhq_production_ci'
            OMNIBUS
          end

          def exec_commands
            [
              "sed -i 's/#gitlab_rails/gitlab_rails/g' /etc/gitlab/gitlab.rb",
              "gitlab-ctl reconfigure",
              "gitlab-psql -c 'create database gitlabhq_production_ci owner gitlab'",
              "gitlab-psql -d gitlabhq_production_ci -c 'create extension btree_gist'",
              "gitlab-psql -d gitlabhq_production_ci -c 'create extension pg_trgm'",
              "gitlab-rake db:structure:load:ci",
              "gitlab-ctl restart"
            ].freeze
          end
        end
      end
    end
  end
end
